import collections
import math
from operator import itemgetter
import nltk
import requests
from bs4 import BeautifulSoup
from nltk import re
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import *

"""
Geoffrey Henderson 
Joe Osborne 
"""

##################################################################################
#Functions
#

"""
Takes a string containing the url of a website as an argument
Uses beautiful soup to extract all the text from the page
Any scripts or styling formatting is then removed from the text
The block of text left, which will just be the content of the page,
is returned
"""
def extractText(url):
    html = requests.get(url).text
    soup = BeautifulSoup(html, "html.parser")
    for script in soup(["script", "style"]):
        script.extract()
    content = soup.get_text()
    return content

"""
Takes a string containing the url of a website and a string containing a metaTag keyword as arguments
Uses beautiful soup to extract all the text from the page
The meta tag is then used to return only the contents inside the tag
"""
def extractMeta(url, metaTag):
    html = requests.get(url).text
    soup = BeautifulSoup(html, "html.parser")
    keyW = ""
    for meta in soup.findAll("meta"):
        metaname = meta.get('name', '').lower()
        metaproperties = meta.get('property', '').lower()

        if metaTag == metaname or metaproperties.find(metaTag) > 0:
            keyW = meta['content'].strip()

    return keyW

"""
Takes a string containing the url of a website as an argument
uses nltks tokenizer to tokenize the meta content for keywords and description.
returns the combined list of keywords and description of the webpage
"""
def getMetaTokens(url):
    webPageKey = extractMeta(url, "keywords")
    tokensPageKey = [word.lower() for word in word_tokenize(webPageKey) if re.search("\w", word)]
    webPageDesc = extractMeta(url, "description")
    tokensPageDesc = [word.lower() for word in word_tokenize(webPageDesc) if re.search("\w", word)]

    # Both desc and keywords in one list
    return tokensPageKey + tokensPageDesc

"""
Takes as an argument the complete list of string tokens of text and the corupus which is the whole text
for each word in the tokens counts its occurrences in the corpus
returns the dictionary where the key is the word and the value is the count
"""
def countWord(tokens,corpus):
    wordCount = {}
    for i in range(0,len(tokens)):
        x=corpus.count(tokens[i])
        wordCount[tokens[i]] = x
    return wordCount

"""
Takes as an argument a list of tuples where the first value is the word adn the second is its tag
It then filters out all the words not in the tags shown in the keytags listed below
Returns a list of only those words
"""
def getWordsOfImportance(words):
    keyTags = ['FW', 'JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS','VB']
    wordsOfImportance = []
    for word, pos in words:
        if pos in keyTags:
            wordsOfImportance.append(word)
    return wordsOfImportance

"""
Takes as an argument the dictionary of terms and their occurences and the complete list of tokens for a url
Then returns a dictionary of the word and its term frequency value
"""
def getTermFrequency(wordCount, completeTokens):
    TF = dict()
    for i in wordCount.keys():
        if wordCount.get(i) > 2:
            x = wordCount.get(i)/len(completeTokens)
            TF[i]= x
    return (TF)

"""
Takes as an argument the id of the document(urL), a list of unique words (occuring in the document) and the dictionary containing keywords identified.
Returns a dictionary where the key is the word and the value is a list of the ids of each document the word occurs in
"""
def linkKeywordToDoc(dictKeywords, uniqueKeywords, id):
    for word in uniqueKeywords:
        dictKeywords.setdefault(word,list()).append(id)
    return dictKeywords

"""
Takes as an argument the list of urls(strings) and a dictionary containing all words from the urls and the documents the word occurs in
Returns a dictionary where key = each word and value = idf value
"""
def IDF (urls, completeKeyWords):
    countOfDocs = len(urls)
    testDict = dict()
    for i in completeKeyWords:
        documentsWithTerm = len(completeKeyWords.get(i))
        diff = countOfDocs/documentsWithTerm
        idf = math.log(diff,10)
        testDict[i] = idf

    return testDict

"""
Takes as an argument a dictionary of keywords and their idf values and a dictionary of dictionaries containing
words and their term frequency
Returns a dictionary of dictionaries where the key is the word is the key and the tfidf is the value
"""
def TFIDF (dictTF, dictIDF):
    tfidfDict = dict()
    for i in dictTF:
        tempDict = dict()
        for word in dictIDF:
            tf = dictTF.get(i).get(word)
            idf = dictIDF.get(word)
            if (tf is not None):
                tfidf = tf*idf
                tempDict[word] = tfidf
        tfidfDict[i] = tempDict
    return tfidfDict

"""
Takes as an argument a dictionary containing a word and its tfidf value
Converts the dictionary into a comma separated string line by line in the format:
ranking,word,tfidf
Returns the string
"""
def getCSVStringFrom(TFIDFDict,id):
    stringSep = ""
    counter = 1
    for word,tfidf in TFIDFDict.items():
        stringSep += "id" + str(id) + ","
        stringSep += str(counter) + ","
        stringSep += word + ","
        stringSep += str(tfidf) + "\n"
        counter +=1
    return stringSep

"""
Takes as an argument a list of all urls used and the file name to be saved.
each line is created using the doc id and the relating URL.
A string is then created concatenating each according line with a new line or comma accordingly
Nothing is returned but a file is saved as a .csv
"""
def writeURLToFile(urls, filename):
    line = ""
    for i in range(0,len(urls)):
        line += "id" + str(i) + ","
        line += urls[i]
        line += "\n"
    file = open(filename + "URLS.csv","w")
    file.write(line)
    return

"""
Takes as an argument a list of all urls used, the file name to be saved and a dictionary of dictionaries where
the key is the doc id and value is another dictionary containing each word and its tfidf value
A string is then created concatenating each according line with a new line or comma accordingly
Nothing is returned but two files are saved, where one is a .txt and one .csv
"""
def writeToCsV(completeKeywords,name):
    line = ""
    for val in completeKeywords:
        OrderedDict = collections.OrderedDict(sorted(completeKeywords.get(val).items(), key=itemgetter(1), reverse = True))
        line += getCSVStringFrom(OrderedDict,val)
        line += "\n"
    file = open(name+"TFIDF.csv","w")
    file.write(line)
    return

"""
Takes two arguments a dictionary where the key is the word and the value is the id's
of each document that the word occurs in. the result is a .csv file written with both
values present and separated by either a new line or a comma.
"""
def createCSVFromKeyLinks(keywordIdLinkDict, name):
    line = ""
    for word in keywordIdLinkDict:
        line += word + ","
        line += "|".join(["id" + str(i) for i in keywordIdLinkDict.get(word)])
        line += "\n"
    file = open(name+ "WordAndDocs.csv", "w")
    file.write(line)
    return

"""
The function below is the one line argument used to run the entire
program and all of its functions, the arguments it takes is the filename
which will get passed to all of the functions that writes the files and a list
of the urls used for extracting the text.
"""
def indexDocumentKeywords(urls,FileName):
    # variables accessed throughout
    completeKeyWord = dict()
    completeTF = dict()

    for i in range(0, len(urls)):
        print("Calculating data for URL","id" + str(i) + ": ",urls[i])
        # Removes all meta content and html information leaving only text
        webPageText = extractText(urls[i])

        # Tokenise the text
        tokensPageText = [word.lower() for word in word_tokenize(webPageText) if re.search("\w", word)]

        # Get the meta content including description and keywords
        tokenMetaContent = getMetaTokens(urls[i])
        # Join meta data with page text to increase occurrences
        tokensPageText = tokensPageText + tokenMetaContent

        # Tag the web page content
        taggedPageText = nltk.pos_tag(tokensPageText)

        # Filters the web page content by their tags eliminating any words of little or no importance twice
        wordsOfImportance = getWordsOfImportance(taggedPageText)
        # Removes stop words from the text
        wordsOfImportance = [i for i in wordsOfImportance if i not in stop]

        # Stem words of importance after 2nd tag
        stemmer = PorterStemmer()
        wordsStemmed = [stemmer.stem(word) for word in wordsOfImportance]
        stemmedCorpus = [stemmer.stem(word) for word in tokensPageText]

        # Sublist for stemmed words
        webPageTextUnique = list(set(wordsStemmed))

        # for all words with occurrences greater than two returns the term frequency
        wordCount = countWord(webPageTextUnique, stemmedCorpus)
        completeTF[i] = getTermFrequency(wordCount, tokensPageText)
        completeKeyWord = linkKeywordToDoc(completeKeyWord, completeTF[i].keys(), i)

    calcIDF = IDF(urls, completeKeyWord)
    calcTFIDF = TFIDF(completeTF, calcIDF)
    writeToCsV(calcTFIDF, FileName)
    createCSVFromKeyLinks(completeKeyWord,FileName)
    writeURLToFile(urls,FileName)
    return completeKeyWord,completeTF,calcIDF,calcTFIDF

##################################################################################

##########################################################################################

#Downloads the prerequisites if they do not already exist.
nltk.download("punkt")
nltk.download("averaged_perceptron_tagger")
nltk.download("stopwords")

#The stop words used when removing them from the lists
stop = set(stopwords.words('english'))

#A list containing the urls of the websites having their keywords identified
urls = ["http://www.telegraph.co.uk/news/2017/02/21/barcelona-police-shoot-driver-speeding-truck-full-gas-cylinders/",
        "http://www.bbc.co.uk/news/world-europe-39037674",
        "http://www.imdb.com/title/tt0298130/",
        "http://www.trustedreviews.com/for-honor-review",
        "http://www.pcgamer.com/overwatch-review/",
        "http://www.bbc.co.uk/news/entertainment-arts-39045406",
        "http://csee.essex.ac.uk/staff/udo/index.html",
        "http://orb.essex.ac.uk/CE/CE306/syllabus.html"]


print(urls,"\n")

FileName = "Output"

CompleteKeyWords,CompleteTF,CompleteIDF,CompleteTFIDF = indexDocumentKeywords(urls,FileName)
